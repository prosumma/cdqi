//
//  CDQI.swift
//  CoreDataQueryInterface
//
//  Created by Gregory Higley on 7/14/16.
//  Copyright © 2016 Gregory Higley. All rights reserved.
//

import Foundation

public protocol Typed {
    static var cdqiStaticType: NSAttributeType { get }
    var cdqiType: NSAttributeType { get }
}

extension Typed {
    public static var cdqiStaticType: NSAttributeType {
        return .undefinedAttributeType
    }
    public var cdqiType: NSAttributeType {
        return self.dynamicType.cdqiStaticType
    }
}

public protocol ExpressionConvertible {
    var cdqiExpression: NSExpression { get }
}

extension NSExpression: ExpressionConvertible {
    public var cdqiExpression: NSExpression {
        return self
    }
}

public protocol TypedExpressionConvertible: ExpressionConvertible, Typed {
    associatedtype CDQIComparisonType: Typed
}

public protocol PredicateComparableTypedExpressionConvertible: TypedExpressionConvertible {}

public protocol TypedConstantExpressionConvertible: TypedExpressionConvertible {}

extension TypedConstantExpressionConvertible {
    public var cdqiExpression: NSExpression {
        return NSExpression(forConstantValue: (self as! AnyObject))
    }
}

extension Int: TypedConstantExpressionConvertible {
    public typealias CDQIComparisonType = NSNumber
#if arch(x86_64) || arch(arm64)
    public static let cdqiStaticType = NSAttributeType.integer64AttributeType
#else
    public static let cdqiStaticType = NSAttributeType.integer32AttributeType
#endif
}

extension Int32: TypedConstantExpressionConvertible {
    public typealias CDQIComparisonType = NSNumber
    public static let cdqiStaticType = NSAttributeType.integer32AttributeType
}

extension Int64: TypedConstantExpressionConvertible {
    public typealias CDQIComparisonType = NSNumber
    public static let cdqiStaticType = NSAttributeType.integer64AttributeType
}

extension Date: TypedConstantExpressionConvertible {
    public typealias CDQIComparisonType = Date
    public static let cdqiStaticType = NSAttributeType.dateAttributeType
}

extension String: TypedConstantExpressionConvertible {
    public typealias CDQIComparisonType = String
    public static let cdqiStaticType = NSAttributeType.stringAttributeType
}

extension Bool: TypedConstantExpressionConvertible {
    public typealias CDQIComparisonType = Bool
    public static let cdqiStaticType = NSAttributeType.booleanAttributeType
}

extension Double: TypedConstantExpressionConvertible {
    public typealias CDQIComparisonType = NSNumber
    public static let cdqiStaticType = NSAttributeType.doubleAttributeType
}

extension NSNumber: TypedConstantExpressionConvertible {
    public typealias CDQIComparisonType = NSNumber
    @nonobjc public static let cdqiStaticType = NSAttributeType.doubleAttributeType
}

extension NSManagedObjectID: TypedConstantExpressionConvertible {
    public typealias CDQIComparisonType = NSManagedObjectID
    @nonobjc public static let cdqiStaticType = NSAttributeType.objectIDAttributeType
}

public protocol KeyPathExpressionConvertible: ExpressionConvertible {
    var cdqiKey: String? { get }
    var cdqiParent: KeyPathExpressionConvertible? { get }
}

extension KeyPathExpressionConvertible {
    private var cdqiRawKeyPath: String? {
        if cdqiKey == nil && cdqiParent == nil {
            return nil
        }
        guard let key = cdqiKey else {
            assertionFailure("Can't build cdqiKeyPath: Missing cdqiKey in KeyPathExpressionConvertible.")
            return nil
        }
        if let parentKeyPath = cdqiParent?.cdqiRawKeyPath {
            return "\(parentKeyPath).\(key)"
        }
        return key
    }
    
    public var cdqiKeyPath: String {
        if let keyPath = cdqiRawKeyPath {
            return keyPath
        }
        return "SELF"
    }
    
    private var cdqiRawName: String? {
        if cdqiKey == nil && cdqiParent == nil {
            return nil
        }
        guard let key = cdqiKey else {
            assertionFailure("Can't build cdqiName: Missing cdqiKey in KeyPathExpressionConvertible.")
            return nil
        }
        if let parentName = cdqiParent?.cdqiRawName {
            let index = key.index(key.startIndex, offsetBy: 1)
            let start = key.substring(to: index)
            let remainder = key.substring(from: index)
            let name = "\(start.uppercased())\(remainder)"
            return "\(parentName)\(name)"
        }
        return key
    }
    
    public var cdqiName: String {
        return cdqiRawName!
    }
    
    public var cdqiExpression: NSExpression {
        return NSExpression(forKeyPath: cdqiKeyPath)
    }
}

public protocol PropertyConvertible {
    var cdqiProperty: AnyObject { get }
}

extension String: PropertyConvertible {
    public var cdqiProperty: AnyObject {
        return self
    }
}

extension NSPropertyDescription: PropertyConvertible {
    public var cdqiProperty: AnyObject {
        return self
    }
}

extension PropertyConvertible where Self: KeyPathExpressionConvertible {
    public var cdqiProperty: AnyObject {
        let property = NSExpressionDescription()
        property.expression = cdqiExpression
        property.name = cdqiName
        if let typed = self as? Typed {
            property.expressionResultType = typed.cdqiType
        }
        return property
    }
}

public enum Function: String {
    case count = "count:"
    case sum = "sum:"
    case stddev = "stddev:"
}

public struct FunctionExpression<E: TypedExpressionConvertible>: PredicateComparableTypedExpressionConvertible, KeyPathExpressionConvertible, PropertyConvertible {
    public typealias CDQIComparisonType = E.CDQIComparisonType
    
    public let function: String
    public let expression: ExpressionConvertible
    
    public let cdqiKey: String?
    public let cdqiParent: KeyPathExpressionConvertible?
    public let cdqiType: NSAttributeType
    
    public init(function: Function, expression: ExpressionConvertible, type: NSAttributeType? = nil) {
        self.function = function.rawValue
        self.expression = expression
        if let type = type {
            cdqiType = type
        } else if let typed = expression as? Typed {
            cdqiType = typed.cdqiType
        } else {
            cdqiType = E.cdqiStaticType
        }
        if let keyPath = expression as? KeyPathExpressionConvertible {
            cdqiParent = keyPath
        } else {
            cdqiParent = nil
        }
        let range = self.function.range(of: ":")!
        cdqiKey = self.function.substring(to: range.lowerBound)
    }
    
    public var cdqiExpression: NSExpression {
        return NSExpression(forFunction: function, arguments: [expression.cdqiExpression])
    }
    
    public var cdqiKeyPath: String {
        assertionFailure("Can't build keypath from function.")
        return ""
    }
}

public func alias(_ expression: ExpressionConvertible, name: String, type: NSAttributeType) -> PropertyConvertible {
    let property = NSExpressionDescription()
    property.expression = expression.cdqiExpression
    property.name = name
    property.expressionResultType = type
    return property
}

public func alias(_ expression: ExpressionConvertible, name: String) -> PropertyConvertible {
    var type: NSAttributeType = .undefinedAttributeType
    if let typed = expression as? Typed {
        type = typed.cdqiType
    }
    return alias(expression, name: name, type: type)
}

public func alias(_ expression: KeyPathExpressionConvertible, type: NSAttributeType) -> PropertyConvertible {
    return alias(expression, name: expression.cdqiName, type: type)
}

public func count(_ expression: ExpressionConvertible) -> FunctionExpression<Int> {
    return FunctionExpression(function: .count, expression: expression, type: Int.cdqiStaticType)
}

public func count(_ expression: ExpressionConvertible, alias name: String) -> PropertyConvertible {
    return alias(count(expression), name: name)
}

public func sum(_ expression: ExpressionConvertible, type: NSAttributeType? = nil) -> FunctionExpression<Double> {
    return FunctionExpression(function: .sum, expression: expression, type: type)
}

public func sum(_ expression: ExpressionConvertible, name: String, type: NSAttributeType? = nil) -> PropertyConvertible {
    let f = sum(expression)
    return alias(f, name: name, type: type ?? f.cdqiType)
}

public func any(_ predicate: Predicate) -> Predicate {
    return Predicate(format: "ANY \(predicate)")
}

public class EntityAttribute: PredicateComparableTypedExpressionConvertible, KeyPathExpressionConvertible, PropertyConvertible {
    public typealias CDQIComparisonType = NSManagedObjectID
    
    public let cdqiKey: String?
    public let cdqiParent: KeyPathExpressionConvertible?

    public required init(key: String? = nil, parent: KeyPathExpressionConvertible? = nil) {
        // You can have a key without a parent, but you can't have a parent without a key.
        assert(key != nil || parent == nil, "Parent without key in Attribute.")
        // If key is a variable, i.e., starts with a $, it can't have a parent.
        assert(!(key?.hasPrefix("$") ?? false) || parent == nil, "Variable with parent in Attribute.")
        cdqiKey = key
        cdqiParent = parent
    }
    
    public required convenience init(variable: String) {
        self.init(key: "$\(variable)")
    }
}

public struct ScalarAttribute<E: TypedExpressionConvertible>: PredicateComparableTypedExpressionConvertible, KeyPathExpressionConvertible, PropertyConvertible {
    public typealias CDQIComparisonType = E.CDQIComparisonType
    
    public let cdqiKey: String?
    public let cdqiParent: KeyPathExpressionConvertible?
    public let cdqiType: NSAttributeType = E.cdqiStaticType
    
    public init(key: String, parent: KeyPathExpressionConvertible) {
        assert(!key.hasPrefix("$"), "Variable used as key for ScalarAttribute.")
        cdqiKey = key
        cdqiParent = parent
    }
}

public typealias StringAttribute = ScalarAttribute<String>
public typealias Integer32Attribute = ScalarAttribute<Int32>
public typealias Integer64Attribute = ScalarAttribute<Int64>
public typealias DoubleAttribute = ScalarAttribute<Double>
public typealias DateAttribute = ScalarAttribute<Date>

public func aggregate<S: Sequence where S.Iterator.Element: ExpressionConvertible>(_ expressions: S) -> ExpressionConvertible {
    let items = expressions.map{ $0.cdqiExpression }
    return NSExpression(forAggregate: items)
}

public func compare(_ lhs: ExpressionConvertible, _ op: ComparisonPredicate.Operator, _ rhs: ExpressionConvertible, options: ComparisonPredicate.Options = []) -> Predicate {
    let (le, re) = (lhs.cdqiExpression, rhs.cdqiExpression)
    return ComparisonPredicate(leftExpression: le, rightExpression: re, modifier: .direct, type: op, options: options)
}

public func equalTo(_ lhs: ExpressionConvertible, _ rhs: ExpressionConvertible, options: ComparisonPredicate.Options = []) -> Predicate {
    return compare(lhs, .equalTo, rhs, options: options)
}

public func greaterThan(_ lhs: ExpressionConvertible, _ rhs: ExpressionConvertible, options: ComparisonPredicate.Options = []) -> Predicate {
    return compare(lhs, .greaterThan, rhs, options: options)
}

public func among<R: Sequence where R.Iterator.Element: ExpressionConvertible>(_ lhs: ExpressionConvertible, _ rhs: R, options: ComparisonPredicate.Options = []) -> Predicate {
    return compare(lhs, .in, aggregate(rhs), options: options)
}

public func ==<L: PredicateComparableTypedExpressionConvertible, R: TypedExpressionConvertible where L.CDQIComparisonType == R.CDQIComparisonType>(lhs: L, rhs: R) -> Predicate {
    return equalTo(lhs, rhs)
}

public func ==<L: PredicateComparableTypedExpressionConvertible, E: TypedExpressionConvertible, R: Sequence where L.CDQIComparisonType == E.CDQIComparisonType, R.Iterator.Element == E>(lhs: L, rhs: R) -> Predicate {
    return among(lhs, rhs)
}

public func ><L: PredicateComparableTypedExpressionConvertible, R: TypedExpressionConvertible where L.CDQIComparisonType == R.CDQIComparisonType>(lhs: L, rhs: R) -> Predicate {
    return greaterThan(lhs, rhs)
}

public func subquery<E: EntityAttribute>(_ items: E, _ query: (E) -> Predicate) -> ExpressionConvertible {
    let uuid = NSUUID().uuidString
    let index = uuid.index(uuid.startIndex, offsetBy: 6)
    let randomString = uuid.substring(to: index)
    let variable = "v\(randomString)"
    return NSExpression(forSubquery: (items as KeyPathExpressionConvertible).cdqiExpression, usingIteratorVariable: variable, predicate: query(E(variable: variable)))
}

public func subqueryCount<E: EntityAttribute>(_ items: E, _ query: (E) -> Predicate) -> FunctionExpression<Int> {
    return count(subquery(items, query))
}

public protocol Result {
    static var cdqiResultType: NSFetchRequestResultType { get }
}

public protocol Entity: Result {
    associatedtype CDQIAttribute: EntityAttribute
}

extension NSManagedObject: Result {
    @nonobjc public static let cdqiResultType = NSFetchRequestResultType.managedObjectResultType
}

extension NSDictionary: Result {
    @nonobjc public static let cdqiResultType = NSFetchRequestResultType.dictionaryResultType
}
extension NSManagedObjectID: Result {
    @nonobjc public static let cdqiResultType = NSFetchRequestResultType.managedObjectIDResultType
}

struct QueryBuilder<M: NSManagedObject where M: Entity> {
    private init() {}
    var resultType: NSFetchRequestResultType = .managedObjectResultType
    var predicates = [Predicate]()
}

public struct Query<M: NSManagedObject, R: Result where M: Entity> {
    private let builder: QueryBuilder<M>
    
    private init(builder: QueryBuilder<M> = QueryBuilder<M>()) {
        self.builder = builder
    }
    
    public static func from(_: M.Type) -> Query<M, M> {
        return Query<M, M>()
    }
    
    public func filter(_ predicate: Predicate) -> Query<M, R> {
        var builder = self.builder
        builder.predicates.append(predicate)
        return Query(builder: builder)
    }
    
    public func filter(_ block: (M.CDQIAttribute) -> Predicate) -> Query<M, R> {
        let predicate = block(M.CDQIAttribute())
        return filter(predicate)
    }
    
    public func refilter() -> Query<M, R> {
        var builder = self.builder
        builder.predicates = []
        return Query(builder: builder)
    }
    
    public func select<P: Sequence where P.Iterator.Element: PropertyConvertible>(_ properties: P) -> Query<M, NSDictionary> {
        return Query<M, NSDictionary>(builder: builder)
    }
    
    public func select(_ properties: PropertyConvertible...) -> Query<M, NSDictionary> {
        return Query<M, NSDictionary>(builder: builder)
    }
    
    public func select(_ block: (M.CDQIAttribute) -> [PropertyConvertible]) -> Query<M, NSDictionary> {
        return Query<M, NSDictionary>(builder: builder)
    }
    
    public func select<P: Sequence where P.Iterator.Element == ((M.CDQIAttribute) -> PropertyConvertible)>(_ blocks: P) -> Query<M, NSDictionary> {
        return Query<M, NSDictionary>(builder: builder)
    }
    
    public func select(_ blocks: ((M.CDQIAttribute) -> PropertyConvertible)...) -> Query<M, NSDictionary> {
        return Query<M, NSDictionary>(builder: builder)
    }
    
    public func select() -> Query<M, NSDictionary> {
        return Query<M, NSDictionary>(builder: builder)
    }
    
    public func objects() -> Query<M, M> {
        return Query<M, M>(builder: builder)
    }

    public func ids() -> Query<M, NSManagedObjectID> {
        return Query<M, NSManagedObjectID>(builder: builder)
    }
    
    public func request() -> NSFetchRequest<M>? {
        return nil
    }
    
    public func all() -> [R] {
        var builder = self.builder
        builder.resultType = R.cdqiResultType
        return []
    }
}

