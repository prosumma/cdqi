//
//  CoreDataQueryInterface.h
//  CoreDataQueryInterface
//
//  Created by Gregory Higley on 7/14/16.
//  Copyright © 2016 Gregory Higley. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for CoreDataQueryInterface.
FOUNDATION_EXPORT double CoreDataQueryInterfaceVersionNumber;

//! Project version string for CoreDataQueryInterface.
FOUNDATION_EXPORT const unsigned char CoreDataQueryInterfaceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreDataQueryInterface/PublicHeader.h>


