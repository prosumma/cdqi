//
//  CoreDataQueryInterfaceTests.swift
//  CoreDataQueryInterfaceTests
//
//  Created by Gregory Higley on 7/14/16.
//  Copyright © 2016 Gregory Higley. All rights reserved.
//

import XCTest
import CoreData
@testable import CoreDataQueryInterface

class Author: NSManagedObject, ManagedEntity {
    typealias CDQIAttribute = AuthorAttribute
}

enum Category: String, TypedExpressionConvertible {
    typealias CDQIComparisonType = String
    case Fiction = "Fiction"
    case NonFiction = "NonFiction"
    var cdqiExpression: NSExpression {
        return NSExpression(forConstantValue: rawValue)
    }
}

class CoreDataQueryInterfaceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        
    }
}

class BookAttribute: EntityAttribute {
    private(set) lazy var title: StringAttribute = { StringAttribute(key: "title", parent: self) }()
    private(set) lazy var publishingDate: DateAttribute = { DateAttribute(key: "date", parent: self) }()
    private(set) lazy var cost: Integer32Attribute = { Integer32Attribute(key: "cost", parent: self) }()
    
}

class AuthorAttribute: EntityAttribute {
    private(set) lazy var name: StringAttribute = { StringAttribute(key: "name", parent: self) }()
    private(set) lazy var books: BookAttribute = { BookAttribute(key: "books", parent: self) }()
}

